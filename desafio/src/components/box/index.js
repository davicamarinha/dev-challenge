import React, { useState } from 'react';
import ReactHtmlParser from 'react-html-parser';

import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

import styles from './index.module.scss';

import { classNames } from 'utils';

const Box = ({ title, text }) => {
    const [show, setShow] = useState(false);

    return (
        <div className={classNames(styles.box, { [styles.show]:show })}>
            <i onClick={()=>setShow(!show)} className={styles.icon}>{show ? <RemoveIcon /> : <AddIcon />}</i>
            <strong>{title}</strong>
            <p>{ReactHtmlParser(text)}</p>
        </div>
    );
};

export default Box;