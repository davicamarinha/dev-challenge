import React, { useState } from 'react';

import { classNames, isMobile } from 'utils';


import MenuIcon from '@material-ui/icons/Menu';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';

import Logo from './images/logo.png';
import Lojas from './images/lojas.png';
import Phone from './images/contato.png';
import User from './images/login.png';
import Sacola from './images/sacola.png';
import Busca from './images/busca.png';

import styles from './index.module.scss';

const Header = () => {
    const [open, setOpen] = useState(false);
    return (
        <header className={styles.header}>
            <div className={styles.logo}>
                <img src={Logo} alt='Logo Animale' />
            </div>
            <div className={styles.menuContainer}>
                <div className={styles.phone}>
                    <img src={Lojas} alt='Lojas' />
                    <img src={Phone} alt='Telefone' />
                </div>
                {isMobile() && (<div onClick={()=>setOpen(!open)} className={styles.mobileBtMenu}>{open ? (<MenuOpenIcon />) : (<MenuIcon />)}</div>)}
                <div className={classNames(styles.menu, {[styles.open]:open })}>
                    <ul>
                        <li>
                            <span>Novidades</span>
                            <div className={styles.subMenu}>
                                <div className={styles.wrapper}>
                                    <div className={classNames(styles.col, styles.categories)}>
                                        <strong>Categorias</strong>
                                        <ul>
                                            <li>Vestido</li>
                                            <li>Top/Blusa</li>
                                            <li>Calça</li>
                                            <li>Camisa</li>
                                            <li>Jaqueta</li>
                                            <li>Macacão</li>
                                            <li>Saia</li>
                                            <li>Short</li>
                                            <li>Blazer</li>
                                            <li>Colete</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                        <ul>
                                            <li>Couro</li>
                                            <li>Seda</li>
                                            <li>Tricot</li>
                                            <li>Club/Noite</li>
                                            <li>Jeans</li>
                                            <li>Malha</li>
                                            <li>Alfaiataria</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                    <div className={classNames(styles.col, styles.accessories)}>
                                        <strong>Acessórios</strong>
                                        <ul>
                                            <li>Bolsas</li>
                                            <li>Calçados</li>
                                            <li>Cinto</li>
                                            <li>Colar</li>
                                            <li>Anel</li>
                                            <li>Brinco</li>
                                            <li>Pulseira</li>
                                            <li>Echarpe/Lenço</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                    <div className={classNames(styles.col, styles.intimates)}>
                                        <strong>Intimates</strong>
                                        <ul>
                                            <li>Lingerie</li>
                                            <li>Underwear</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <span>Coleção</span>
                            <div className={styles.subMenu}>
                                <div className={styles.wrapper}>
                                    <div className={classNames(styles.col, styles.categories)}>
                                        <strong>Categorias</strong>
                                        <ul>
                                            <li>Vestido</li>
                                            <li>Top/Blusa</li>
                                            <li>Calça</li>
                                            <li>Camisa</li>
                                            <li>Jaqueta</li>
                                            <li>Macacão</li>
                                            <li>Saia</li>
                                            <li>Short</li>
                                            <li>Blazer</li>
                                            <li>Colete</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                        <ul>
                                            <li>Couro</li>
                                            <li>Seda</li>
                                            <li>Tricot</li>
                                            <li>Club/Noite</li>
                                            <li>Jeans</li>
                                            <li>Malha</li>
                                            <li>Alfaiataria</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                    <div className={classNames(styles.col, styles.accessories)}>
                                        <strong>Acessórios</strong>
                                        <ul>
                                            <li>Bolsas</li>
                                            <li>Calçados</li>
                                            <li>Cinto</li>
                                            <li>Colar</li>
                                            <li>Anel</li>
                                            <li>Brinco</li>
                                            <li>Pulseira/Bracelete</li>
                                            <li>Echarpe/Lenço</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                    <div className={classNames(styles.col, styles.intimates)}>
                                        <strong>Intimates</strong>
                                        <ul>
                                            <li>Lingerie</li>
                                            <li>Underwear</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <span>Joias</span>
                            <div className={styles.subMenu}>
                                <div className={styles.wrapper}>
                                    <div className={classNames(styles.col, styles.categories)}>
                                        <strong>Categorias</strong>
                                        <ul>
                                            <li>Vestido</li>
                                            <li>Top/Blusa</li>
                                            <li>Calça</li>
                                            <li>Camisa</li>
                                            <li>Jaqueta</li>
                                            <li>Macacão</li>
                                            <li>Saia</li>
                                            <li>Short</li>
                                            <li>Blazer</li>
                                            <li>Colete</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                        <ul>
                                            <li>Couro</li>
                                            <li>Seda</li>
                                            <li>Tricot</li>
                                            <li>Club/Noite</li>
                                            <li>Jeans</li>
                                            <li>Malha</li>
                                            <li>Alfaiataria</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                    <div className={classNames(styles.col, styles.accessories)}>
                                        <strong>Acessórios</strong>
                                        <ul>
                                            <li>Bolsas</li>
                                            <li>Calçados</li>
                                            <li>Cinto</li>
                                            <li>Colar</li>
                                            <li>Anel</li>
                                            <li>Brinco</li>
                                            <li>Pulseira/Bracelete</li>
                                            <li>Echarpe/Lenço</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                    <div className={classNames(styles.col, styles.intimates)}>
                                        <strong>Intimates</strong>
                                        <ul>
                                            <li>Lingerie</li>
                                            <li>Underwear</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <span>Sale</span>
                            <div className={styles.subMenu}>
                                <div className={styles.wrapper}>
                                    <div className={classNames(styles.col, styles.categories)}>
                                        <strong>Categorias</strong>
                                        <ul>
                                            <li>Vestido</li>
                                            <li>Top/Blusa</li>
                                            <li>Calça</li>
                                            <li>Camisa</li>
                                            <li>Jaqueta</li>
                                            <li>Macacão</li>
                                            <li>Saia</li>
                                            <li>Short</li>
                                            <li>Blazer</li>
                                            <li>Colete</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                        <ul>
                                            <li>Couro</li>
                                            <li>Seda</li>
                                            <li>Tricot</li>
                                            <li>Club/Noite</li>
                                            <li>Jeans</li>
                                            <li>Malha</li>
                                            <li>Alfaiataria</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                    <div className={classNames(styles.col, styles.accessories)}>
                                        <strong>Acessórios</strong>
                                        <ul>
                                            <li>Bolsas</li>
                                            <li>Calçados</li>
                                            <li>Cinto</li>
                                            <li>Colar</li>
                                            <li>Anel</li>
                                            <li>Brinco</li>
                                            <li>Pulseira/Bracelete</li>
                                            <li>Echarpe/Lenço</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                    <div className={classNames(styles.col, styles.intimates)}>
                                        <strong>Intimates</strong>
                                        <ul>
                                            <li>Lingerie</li>
                                            <li>Underwear</li>
                                            <li>Ver tudo ></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div className={styles.cart}>
                    <img src={User} alt='Login' />
                    <img src={Sacola} alt='Sacola' />
                    <img src={Busca} alt='Busca' />
                </div>
            </div>
        </header>
    )
}

export default Header;
