import React from 'react';
import Header from './index';

import { render } from '@testing-library/react'

test('Header Snapshot', () => {
  const tree = render(<Header />);
  expect(tree).toMatchSnapshot();
});