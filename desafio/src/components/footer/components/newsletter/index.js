import React from 'react';
import { TextField, Button } from '@material-ui/core';

import styles from './index.module.scss';

const Newsletter = () => {
    return (
        <div className={styles.news}>
            <div className={styles.wrapper}>
                <div className={styles.text}>
                    <strong>Newsletter</strong>
                    <span>Cadastre-se para receber nossas novidades e promoções</span>
                </div>
                <div  className={styles.fields}>
                    <TextField placeholder='Nome' className={styles.name}/>
                    <TextField placeholder='E-mail' className={styles.email}/>
                    <Button className={styles.button}>OK</Button>
                </div>
            </div>
        </div>
    );
};

export default Newsletter;
