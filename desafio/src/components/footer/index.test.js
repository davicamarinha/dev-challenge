import React from 'react';
import Footer from './index';

import { render } from '@testing-library/react'

test('Footer Snapshot', () => {
  const tree = render(<Footer />);
  expect(tree).toMatchSnapshot();
});