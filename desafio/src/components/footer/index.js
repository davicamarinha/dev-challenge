import React from 'react';

import Newsletter from './components/newsletter/';

import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

import styles from './index.module.scss';

const Footer = () => {
    return (
        <footer className={styles.footer}>
            <Newsletter />
            <div className={styles.menu}>
                <KeyboardArrowDownIcon className={styles.icon} />
                <ul>
                    <li>A marca</li>
                    <li>Minha conta</li>
                    <li>Políticas</li>
                    <li>Formas de pagamento</li>
                    <li>Newsletter</li>
                </ul>
            </div>
            <small>RBX RIO COMÉRCIO DE ROUPAS LTDA. ESTRADA DOS BANDEIRANTES, 1.700 - GALPÃO 03, ARMAZÉM 104 - TAQUARA, RIO DE JANEIRO, RJ - CEP: 22775-109. CNPJ: 10.285.590/0002-80.</small>
        </footer>
    );
};

export default Footer;