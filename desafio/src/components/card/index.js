import React from 'react';
import { Link } from "react-router-dom";

import styles from './index.module.scss';

const Card = ({ image, title, price, installment, id }) => {
    return (
        <div className={styles.card}>
            <img src={image} alt={title} className={styles.img} />
            <div className={styles.info}>
                <span className={styles.name}>{title}</span>
                <div className={styles.priceBox}>
                    <strong className={styles.price}>{price.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</strong>
                    <span className={styles.installment}>{`${installment}x de ${parseFloat(price / installment).toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}`}</span>
                </div>
                <Link className={styles.quick} to={`/product/${id}`}>Quick Shop</Link>
            </div>
        </div>
    );
};

export default Card;
