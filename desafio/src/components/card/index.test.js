import React from 'react';
import Card from './index';
import { BrowserRouter } from 'react-router-dom';

import { render } from '@testing-library/react'

test('Footer Snapshot', () => {
    const fakedata =[{
        image:'https://img.itdg.com.br/tdg/images/recipes/000/155/011/322741/322741_original.jpg?w=1200',
        title:'teste',
        price: 13,
        installment: 1,
        id: 1234
    }];
    const tree = render(<BrowserRouter><Card image={fakedata.image} title={fakedata.title} price={10} installment={1} id={fakedata.id} /></BrowserRouter>);
    expect(tree).toMatchSnapshot();
});