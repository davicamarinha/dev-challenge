import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Header from 'components/header';
import Footer from 'components/footer';
import Card from 'components/card';

import styles from './index.module.scss';

const Categoria = () => {
    const [productList, setProductList] = useState([]);
    const [productShow, setProductShow] = useState([]);
    const [page, setPage] = useState(1); 
    const [listLength, setListLength] = useState(0);
    const [endFile, setEndFile] = useState(true);
    const showPerPage = 8;
    const end = Math.ceil(listLength / showPerPage);

    useEffect(() => {
        productListLoader();
    }, []);

    const productListLoader = async () => {
        await axios.get('products.json').then(res => {
            const productApi = res.data;
            setProductList([productApi]);
            setProductShow(productApi.slice(0, showPerPage));
            setListLength(productApi.length);
        });
    };

    const handleClickShowMore = () => {
        page === end && setEndFile(false);
        if (page <= end) {
            const firstNumber = page + showPerPage;
            const lastNumber = firstNumber + showPerPage;
            setProductShow([...productShow, ...productList[0].slice(firstNumber, lastNumber)]);
            console.log(page)
            setPage(page + 1);
        } 
    }

    return (
        <>
            <Header />
            <main className={styles.main}>
                <div className={styles.shelf}>
                    {productShow.map((product, key) => <Card {...{key}} image={product.items[0].images[0].imageUrl} title={product.productName} price={product.items[0].sellers[0].commertialOffer.Price} installment={10} id={product.productId} />)}
                </div>
                {endFile && (<div onClick={() => handleClickShowMore()} className={styles.btShowMore}>Carregar mais</div>)}
            </main>
            <Footer />
        </>
    );
};

export default Categoria;