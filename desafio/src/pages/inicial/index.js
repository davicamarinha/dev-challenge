import React from 'react';

import Header from 'components/header';
import Footer from 'components/footer';
import SpecialBanner from './components/specialBanner';
import Fullbanner from './components/fullBanner';
import SmallBanner from './components/smallBanner';

import styles from './index.module.scss';

import Banner1 from './images/banner-home-5.png';
import Banner2 from './images/banner-home.png';
import Banner3 from './images/banner-home-1.png';
import Banner4 from './images/banner-home-2.png';
import Banner5 from './images/banner-home-3.png';
import Banner6 from './images/banner-home-4.png';

const mockData = [
    {
        'image': Banner4,
        'title': 'Lunar',
        'text': 'De Sevilla, capital do flamenco, o vestuário típico da dança mais celebrada da Espanha inspira as novas peças da coleção.'
    },{
        'image': Banner5,
        'title': 'Flutua',
        'text': 'A natureza dos célebres jardins espanhóis dá vida às estampas florais'
    },{
        'image': Banner6,
        'title': 'FLASH LIGHTS',
        'text': 'O último editorial também se inspira em Barcelona, cidade iluminada pelo sol. Conheça as cores que são aposta da coleção.'
    }
];

const Inicial = () => {
    return (
        <>
            <Header />
            <main className={styles.main}>
                <SpecialBanner image={Banner2} title='Go Modern' text='Nada óbvios, os shapes são contemporâneos com recortes estratégicos e estampas abstratas.' reverse={false} />
                <SpecialBanner image={Banner1} title='CORES DA CIDADE' text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.' reverse />
                <Fullbanner image={Banner3} title='Inside Animale' text='Fique por dentro do Universo Animale com apenas um clique.' />
                <div className={styles.smallBannerContainer}>
                    {mockData.map((mockup) => <SmallBanner image={mockup.image} title={mockup.title} text={mockup.text} />)}
                </div>
            </main>
            <Footer />
        </>
    );
};

export default Inicial;
