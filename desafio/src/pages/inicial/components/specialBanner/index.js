import React from 'react';
import { Link } from "react-router-dom";

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

import { classNames, isMobile } from 'utils';

import styles from './index.module.scss';

const SpecialBanner = ({ image, title, text, reverse }) => {
    return (
        <>
            {isMobile ? (
                <div className={classNames(styles.mobile, styles.specBan, {[styles.reverse]:reverse})}>
                    <Carousel 
                        autoPlay
                        infiniteLoop
                        showArrows={false} 
                        showStatus={false} 
                        showThumbs={false} 
                        showIndicators={false}>
                        <div className={styles.textBox}>
                            <div className={styles.wrapper}>
                                <strong>{title}</strong>
                                <span>{text}</span>
                                <div className={styles.shopNow}><Link to="/catalog">Shop Now</Link></div>
                            </div>
                        </div>
                        <div className={styles.imgBox}>
                            <img src={image} alt={title} />
                        </div>
                    </Carousel>
                </div>
            ) : (
                <div className={classNames(styles.specBan, {[styles.reverse]:reverse} )}>
                    <div className={styles.textBox}>
                        <div className={styles.wrapper}>
                            <strong>{title}</strong>
                            <span>{text}</span>
                            <div className={styles.shopNow}><Link to="/catalog">Shop Now</Link></div>
                        </div>
                    </div>
                    <div className={styles.imgBox}>
                        <img src={image} alt={title} />
                    </div>
                </div>
            )}
        </>
    );
};

export default SpecialBanner;