import React from 'react';
import { Link } from "react-router-dom";

import { classNames, isMobile } from 'utils';

import styles from './index.module.scss';

const FullBanner = ({ image, title, text }) => {
    return (
        <>
            <div className={styles.fullbanner}>
                <img src={image} alt={title} />
                <div className={styles.wrapper}>
                    <strong>{title}</strong>
                    <span>{text}</span>
                    <div className={styles.shopNow}><Link to="/catalog">Shop Now</Link></div>
                </div>
            </div>
        </>
    );
};

export default FullBanner;