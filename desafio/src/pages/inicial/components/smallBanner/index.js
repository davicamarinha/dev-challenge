import React from 'react';
import { Link } from "react-router-dom";

import styles from './index.module.scss';

const SmallBanner = ({ image, title, text }) => {
    return (
        <div className={styles.smallBanner}>
            <img src={image} alt={title} />
            <div className={styles.wrapper}>
                <strong>{title}</strong>
                <span>{text}</span>
                <div className={styles.shopNow}><Link to="/catalog">Shop Now</Link></div>
            </div>
        </div>
    );
};

export default SmallBanner;
