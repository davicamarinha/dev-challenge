import React, { useState, useEffect } from 'react';
import axios from 'axios';

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

import Header from 'components/header';
import Footer from 'components/footer';
import Box from 'components/box';

import { classNames, isMobile } from 'utils';

import styles from './index.module.scss';

const Produto = (props) => {
    const { match } = props;
    const id = match.params.id;

    const [product, setProduct] = useState([]);
    
    const productLoader = async () => {
        await axios.get('http://localhost:3000/products.json').then(res => {
            const productApi = res.data;
            for (let x = 0; x < productApi.length; x++) {
                productApi[x].productId === id && setProduct(productApi[x]); 
            }
        });
    };

    useEffect(() => {
        productLoader();
    }, []);

    return (
        <>  
            <Header />
            {product.items !== undefined && (
                <main className={styles.main}>
                    {console.log(product)}
                    <div className={styles.imageSide}>
                        {isMobile ? <Carousel 
                        autoPlay
                        infiniteLoop
                        showArrows={false} 
                        showStatus={false} 
                        showThumbs={false} 
                        showIndicators={false}>{product.items[0].images.map((image, key) => <img {...{key}} alt={image.complementName} src={image.imageUrl} />)}</Carousel> : product.items[0].images.map((image, key) => <img {...{key}} alt={image.complementName} src={image.imageUrl} />)}
                    </div>
                    <div className={styles.infoSide}>
                        <div className={styles.info}>
                            <small>Ref.: {product.productReference}</small>
                            <h1>{product.productName}</h1>
                            <div className={styles.price}>
                                <strong>{product.items[0].sellers[0].commertialOffer.Price.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</strong>
                                <span>ou 10x de {parseFloat(product.items[0].sellers[0].commertialOffer.Price / 10).toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</span>
                            </div>
                            <div className={styles.skus}>
                                <span className={styles.tamanho}>
                                    <div className={styles.title}>Tamanho</div>
                                    <div className={styles.sku}>
                                        {product.skuSpecifications[0].values.map((sku, key) => <span {...{key}}>{sku.name}</span>)}
                                    </div>
                                </span>
                            </div>
                            <div className={styles.buyButton}>Adicionar à Sacola</div>
                            <div className={styles.details}>
                                <Box title="Composição" text={product.Composição}/>
                                <Box title="Descrição" text={product.description}/>
                            </div>
                        </div>
                    </div>
                </main>
            )}
            <Footer />
        </>
    );
};

export default Produto;