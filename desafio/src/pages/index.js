import Inicial from './inicial/';
import Categoria from './categoria/';
import Produto from './produto/';

export { Inicial, Categoria, Produto };