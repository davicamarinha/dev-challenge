import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import ReactDOM from 'react-dom';

import './index.css';

import { Inicial, Categoria, Produto } from './pages';

ReactDOM.render(
  <Router>
    <Switch>
      <Route path="/" exact component={Inicial} />
      <Route path="/catalog" exact component={Categoria} />
      <Route path="/product/:id" exact component={Produto} />
    </Switch>
  </Router>,
  document.getElementById('root')
);