## Iniciando

Depois de ter baixado o repositório, baixei as dependencias (se tiver no Linux, não esqueça de usar o SUDO antes)

### `npm install` ou `yarn`

Depois de tudo instalado, inicialize o projeto

### `npm start` ou `yarn start`

Abra no seu navegador [http://localhost:3000](http://localhost:3000).

Se quiser rodar os teste... (só consegui fazer uns 3, 2 simples e um mais complexo)
### `npm test` ou `yarn test`

Se quiserem ver meu [Github](https://github.com/triblood). 