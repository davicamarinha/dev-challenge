# Desafio FrontEnd

## Instruções para o teste
* Dê um fork neste projeto;
* Desenvolva as telas;
* Atualize o readme com as instruções necessárias para rodar o seu código;
* Faça um pull request.


##### Sugestões de implementação
* Interação com JSON para renderizar os produtos (você vai encontrar um mockup em src/data/products.json)
* Filtro de produtos funcional
* Adicionar produtos ao carrinho
* Botão de carregar mais produtos

##### Dicas
* Evite usar linguagens, ferramentas e metodologias que não domine;
* Não esqueça de manter o package atualizado com os módulos necessários para rodar seu projeto;


###### Dúvidas: vinicius.diniz@somagrupo.com.br